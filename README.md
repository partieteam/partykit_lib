![Untitled-1.jpg](https://bitbucket.org/repo/ARypGG/images/2703873679-Untitled-1.jpg)

# PARTYKit (Version 1.0 Beta) #

This repository contains the PARTYKit that allows you to access PARTY Service Platform from your iOS app.


**Setup your PARTYKit**

*Important*

- In Build Settings add "-all_load" value in "Other linker flag"

In ViewController.h

```
#!objective-c
#import "PARTYKit.h"

@interface ViewController : UIViewController<PARTYKitDelegate>

```

Implement on ViewController.m
```
#!objective-c

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupPARTY];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)setupPARTY{
    _partyKit = [PARTYKit sharedInstance];
    [_partyKit setupWithKEY:KEY
                       host:HOSTADDRESS
                       port:PORT
                     useSSL:NO
                   userInfo:nil
                   delegate:self];
    
    [_partyKit setRecognitionMode:PKRecogitionASRMode];
    [_partyKit setEnableMeter:NO];
}

```

**Action Control**
```
#!objective-c
#pragma mark - Action

- (IBAction)onRecPressed:(id)sender {
    if (_partyKit.isDectection) {
        [_partyKit endDetection];
    }else{
        [_partyKit startDetection];
    }
}

```

**Handler with PARTYDelegate**
```
#!objective-c
#pragma mark - Recognizer Delegate

- (void)recognizerFinishSendingPackage:(PARTYKit *)recognizer{
    
}

- (void)recognizer:(PARTYKit *)recognizer didReceiveResult:(PKRecognizeResponse *)response{
    if (response.isOK) {
        NSLog(@"Result %@", response.result);
    }
}

- (void)recognizer:(PARTYKit *)recognizer didError:(PKError *)error{
    NSLog(@"Error %@", error.description);
}

- (void)recognizerDidConnected:(PARTYKit *)recognizer{
    //Set UIConnected State
}

- (void)recognizerDidDestroyed:(PARTYKit *)recognizer{
    //Set UIDisconnected State
}

```

# Report Issues/Bugs #
## [Issue](https://bitbucket.org/sptspeech-ios/partykit_lib/issues) ##