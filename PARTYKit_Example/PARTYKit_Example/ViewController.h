//
//  ViewController.h
//  PARTYKit_Example
//
//  Created by Kiettiphong Manovisut on 7/1/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PARTYKit.h"
#import "NSDictionary+Json.h"

@interface ViewController : UIViewController<PARTYKitDelegate, UITextFieldDelegate>

@property (nonatomic) PARTYKit *partyKit;

@property (strong, nonatomic) IBOutlet UITextField *tf_ip;
@property (strong, nonatomic) IBOutlet UITextField *tf_port;
@property (strong, nonatomic) IBOutlet UITextView *tv_editor;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (strong, nonatomic) IBOutlet UIButton *btn_record;

- (IBAction)onRecPressed:(id)sender;

@end
