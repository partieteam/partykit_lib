//
//  AppDelegate.h
//  PARTYKit_Example
//
//  Created by Kiettiphong Manovisut on 7/1/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
