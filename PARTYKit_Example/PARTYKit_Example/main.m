//
//  main.m
//  PARTYKit_Example
//
//  Created by Kiettiphong Manovisut on 7/1/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
