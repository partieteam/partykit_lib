//
//  ViewController.m
//  PARTYKit_Example
//
//  Created by Kiettiphong Manovisut on 7/1/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_loading stopAnimating];
    [self setupPARTY];
    
    NSLog(@"%@", [self applicationDocumentsDirectory]);
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated{
    
}

- (void)setupPARTY{
    
    if (!_partyKit) {
        _partyKit = [PARTYKit sharedInstance];
    }
    
    if ([_partyKit isConnected]) {
        [_partyKit destroy];
    }
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:@"email@thailand.com" forKey:@"email"];
    
    NSString *ip = ([_tf_ip.text isEqualToString:@""]) ? @"203.185.132.56" : _tf_ip.text;
    NSInteger port = ([_tf_port.text isEqualToString:@""]) ? 9999 : [_tf_port.text integerValue];
    
    _partyKit = [PARTYKit sharedInstance];
    [_partyKit setupWithKEY:@"4c9184f37cff01bcdc32dc486ec36961"
                       host:ip
                       port:port
                     useSSL:NO
                   userInfo:params
                   delegate:self];
    
    [_partyKit setRecognitionMode:PKRecogitionASRMode];
    [_partyKit setEnableMeter:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        if (textField == _tf_ip) {
            [_tf_ip endEditing:YES];
        }else if (textField == _tf_port){
            [_tf_port endEditing:YES];
        }
        
        [self setupPARTY];
    }
    
    return YES;
}

#pragma mark - Recognizer Delegate

- (void)recognizerFinishSendingPackage:(PARTYKit *)recognizer{
    [_loading startAnimating];
}

- (void)recognizer:(PARTYKit *)recognizer didReceiveResult:(PKRecognizeResponse *)response{
    [_loading stopAnimating];
    if (response.isOK) {
        NSLog(@"Result: %@\n(%@)", response.result, response.resultAsArray);
        [_tv_editor setText:response.result];
    }
}

- (void)recognizer:(PARTYKit *)recognizer didError:(PKError *)error{
    NSLog(@"Error: %@", error.description);
    [_loading stopAnimating];
    if (error.type == PKErrorTypeAuthenticationFail) {
        //Authentication fail
    }
}

- (void)recognizerDidConnected:(PARTYKit *)recognizer{
    //Set UIConnected State
    [self setConnectState];
}

- (void)recognizerWillConnecting:(PARTYKit *)recognizer{
    //Set UIConnecting State
    [self setDisconnectState];
}

- (void)recognizerDidDestroyed:(PARTYKit *)recognizer{
    //Set UIDisconnected State
    [self setDisconnectState];
}

- (void)setConnectState{
    [_loading stopAnimating];
    [_btn_record setTitle:NSLocalizedString(@"Record", @"Record") forState:UIControlStateNormal];
    [_btn_record setBackgroundColor:[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1]];
}

- (void)setDisconnectState{
    [_loading stopAnimating];
    [_btn_record setTitle:NSLocalizedString(@"Connecting...", @"Connecting") forState:UIControlStateNormal];
    [_btn_record setBackgroundColor:[UIColor redColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (IBAction)onRecPressed:(id)sender {
    if (_partyKit.isDectection) {
        [_partyKit endDetection];
        [_btn_record setTitle:@"Record" forState:UIControlStateNormal];
    }else{
        [_partyKit startDetection];
        [_btn_record setTitle:@"Recording..." forState:UIControlStateNormal];
    }
}

- (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

@end
