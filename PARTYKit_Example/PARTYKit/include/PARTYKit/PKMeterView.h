//
//  PKMeterView.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/12/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKAudioUnit.h"

@interface PKMeterView : UIView

@property (nonatomic) AudioQueueRef aq;
@property (nonatomic) CGRect innerRect;
@property (nonatomic) NSTimer *timer;

- (void)setAudioQueue:(AudioQueueRef)aq;

@end
