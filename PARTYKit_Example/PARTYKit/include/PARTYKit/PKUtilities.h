//
//  PKUtilities.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/18/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKUtilities : NSObject

+ (NSString *)getUUID;

@end
