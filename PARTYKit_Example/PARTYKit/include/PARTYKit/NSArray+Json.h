//
//  NSArray+Json.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/17/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray_Json : NSArray

- (NSString*) decodeJsonString:(BOOL)prettyPrint;

@end
