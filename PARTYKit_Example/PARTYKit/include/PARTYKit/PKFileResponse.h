//
//  VoicemailResponse.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/6/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKFileResponse : NSObject

@property (assign) BOOL isOK;
@property (nonatomic, strong) NSString *filename;

- (id)initWithDictionary:(NSDictionary *)jsonObject;

@end
