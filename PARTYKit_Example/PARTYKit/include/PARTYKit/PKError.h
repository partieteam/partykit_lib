//
//  PKError.h
//  PARTYKit
//
//  Created by Kiettiphong Manovisut on 3/11/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
    PKErrorTypeIdle             = 0,
    PKErrorTypeRecognize        = 1,
    PKErrorTypeTimeout          = 2,
    PKErrorTypeAuthenticationFail   = 3,
    PKErrorTypeKeyExpired       = 4,
    PKErrorTypeOutOfService     = 5,
    PKErrorTypeRequestLimit     = 6
}; typedef NSUInteger PKErrorType;

@interface PKError : NSObject

@property (nonatomic) PKErrorType type;
@property (nonatomic, strong) NSString *description;

- (id)initWithDictionary:(NSDictionary *)jsonObject;
- (id)initWithPKErrorType:(PKErrorType)errorType;

@end
