//
//  ASRResponse.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/6/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKRecognizeResponse : NSObject

@property (assign) BOOL isOK;
@property (nonatomic, strong) NSString *result;
@property (nonatomic, strong) NSMutableArray *resultAsArray;

- (id)initWithDictionary:(NSDictionary *)jsonObject;
- (NSMutableArray *)parserMessageAsArray:(NSString *)msg;

@end
