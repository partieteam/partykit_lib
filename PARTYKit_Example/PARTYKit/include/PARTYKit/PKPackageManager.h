//
//  ASRPackageManager.h
//  ThaiASR
//
//  Created by Kiettiphong Manovisut on 5/23/13.
//  Copyright (c) 2013 SPTDEV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <dispatch/dispatch.h>

#import "GCDAsyncSocket.h"

#import "PKEnum.h"

@protocol PKPackageManagerDelegate;

@interface PKPackageManager : NSObject{
    dispatch_queue_t queue;
}

@property (strong, nonatomic) id<PKPackageManagerDelegate> delegate;
@property (strong, nonatomic) GCDAsyncSocket *tcpSocket;
@property (strong, nonatomic) NSMutableArray *metaQueue;

@property (nonatomic) PKRecogitionMode mode;
@property (assign, nonatomic) double rate;
@property (assign, nonatomic) double timeout;
@property (assign, nonatomic) int isStarted;

/**
 * PKPackageDelegate uses the standard delegate paradigm,
 * and this delegate will given process went package is finish sending.
 *
 * You MUST set a delegate in init method
 *
 **/

- (id)initWithSocket:(GCDAsyncSocket*)tcpSocket
                mode:(PKRecogitionMode)mode
          sampleRate:(double)rate
            delegate:(id<PKPackageManagerDelegate>)delegate;

/*!
 @abstract This method manage buffer frame for sending to party server
 *
 */

- (void)addObject:(NSString *)base64;
- (void)startPackagePublish;
- (void)stopPackagePublish;

- (void)echoStream;

/*!
 @abstract This method broadcast string to stream socket
 *
 @discussion it will sending string mode to party server went echoStream end of stream
 and it will call packageDidFinishSending for send package status to delegate view
 */

- (int)getIsStarted;
- (void)setIsStarted:(int)isStart;

@end

enum {
    PKPackageManagerUnknownError    = 0,
    PKPackageManagerFail            = 1,
    PKPackageManagerTimeout         = 2
}; typedef NSUInteger PKPackageManagerErrorType;

/**
 * PKPackageDelegate uses the standard delegate paradigm,
 * and this delegate will given process went package is finish sending.
 *
 * You MUST set a delegate in init method
 *
 **/

@protocol PKPackageManagerDelegate <NSObject>

- (void)packageDidFinishSending:(PKPackageManager *)pkg;

@optional

/*!
 @abstract Sent when the PKPackageManager receive error message
 
 @param error is type of PKPackageManagerErrorType
 
 @discussion This will send type and message of PKPackageManagerErrorType
 */

- (void)package:(PKPackageManager *)pkg didError:(PKPackageManagerErrorType)error;

@end