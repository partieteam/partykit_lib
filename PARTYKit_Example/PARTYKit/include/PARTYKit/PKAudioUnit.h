/*
 Erica Sadun, http://ericasadun.com
 iPhone Developer's Cookbook, 3.0 Edition
 BSD License, Use at your own risk
 */

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>
#import "PKPackageManager.h"
#import "PARTYKit.h"

#define NUM_BUFFERS 3
#define kAudioConverterPropertyMaximumOutputPacketSize		'xops'
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

typedef struct
{
    AudioFileID                 audioFile;
    AudioStreamBasicDescription dataFormat;
    AudioQueueRef               queue;
    AudioQueueBufferRef         buffers[NUM_BUFFERS];
    UInt32                      bufferByteSize;
    SInt64                      currentPacket;
    BOOL                        recording;
    float                       sampleRate;
} RecordState;

PKPackageManager *packageManager;

@interface PKAudioUnit : NSObject {
	RecordState recordState;
}

@property (nonatomic) id<PARTYKitDelegate> delegate;
@property (nonatomic, readonly) BOOL isRecording;
@property (nonatomic, readonly) CGFloat averagePower;
@property (nonatomic, readonly) CGFloat peakPower;
@property (nonatomic, readonly) NSTimeInterval currentTime;
@property (nonatomic) NSString *soundPath;

- (id)initWithSampleRate:(float)rate packageManager:(PKPackageManager*)pack meter:(BOOL)meter partyDelegate:(id<PARTYKitDelegate>)pkDelegate;

- (AudioQueueRef)getAudioQueue;
- (NSString *)getSoundPath;

- (BOOL)	startRecording;
- (void)	stopRecording;
- (void)	pause;
- (BOOL)	resume;

@end
