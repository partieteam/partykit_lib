//
//  PKEnum.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/18/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#ifndef PARTY_PKEnum_h
#define PARTY_PKEnum_h

enum {
    PKRecogitionASRMode            = 0,
    PKRecogitionVoicemail          = 1
}; typedef NSUInteger PKRecogitionMode;

#endif
