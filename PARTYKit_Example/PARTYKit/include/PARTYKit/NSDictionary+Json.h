//
//  NSDictionary+JSON.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/17/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Json)

- (NSString*) decodeJsonString:(BOOL)prettyPrint;

@end
