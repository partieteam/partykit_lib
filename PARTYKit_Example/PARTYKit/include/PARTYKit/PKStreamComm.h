//
//  PKStreamComm.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/18/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKEnum.h"

@interface PKStreamComm : NSObject

+ (NSString *)getAuthFormatWithAPIKey:(NSString *)apiKey
                             deviceId:(NSString *)deviceId
                               params:(NSDictionary *)params;

+ (NSString *)getStartFormatWithRecognitionMode:(PKRecogitionMode)mode
                                     sampleRate:(int)sampleRate;

+ (NSString *)getStreamFormat:(NSString *)base64;

+ (NSString *)getStopFormat;

+ (NSString *)getVoicemailConfirmFormat:(NSString *)sendNumber
                          receiveNumber:(NSString *)receiveNumber;

+ (NSString *)getASRSubmitFormat:(NSString *)text;

+ (NSString *)getExitFormat;

@end
