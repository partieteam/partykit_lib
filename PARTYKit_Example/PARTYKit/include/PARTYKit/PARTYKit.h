//
//  PARTYKit.h
//  PARTY
//
//  Created by Kiettiphong Manovisut on 2/6/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioQueue.h>

// External
#import "GCDAsyncSocket.h"

// Internal
#import "PKPackageManager.h"
#import "PKError.h"
#import "PKEnum.h"

// Response
#import "PKRecognizeResponse.h"
#import "PKFileResponse.h"

#define REC_FILENAME @"record_tmp"
#define REC_FILEEXTENDTION @"raw"

@protocol PARTYKitDelegate;

@interface PARTYKit : NSObject <GCDAsyncSocketDelegate, PKPackageManagerDelegate>

@property (nonatomic, strong) PKPackageManager *pkg;
@property (nonatomic, strong) id<PARTYKitDelegate> delegate;
@property (nonatomic) NSString *soundPath;

@property (assign) BOOL enableMeter;

+ (PARTYKit*) sharedInstance;

/*!
 @abstract This method configures the PARTYKit subsystems.
 
 @param Key Application identification
 @param host PARTY server hostname or IP address
 @param port PARTY server port
 @param useSSL Whether or not PARTY should use SSL to
 communicate with the PARTY server
 @param delegate The receiver for the setup message responses.  If the user
 wishes to destroy and re-connect to the PARTY servers, the delegate must
 implement the PARTYKitDelegate protocol and observe the destroyed method
 as described below.
 
 */
- (void)setupWithKEY:(NSString *)key
                host:(NSString *)host
                port:(int)port
              useSSL:(BOOL)ssl
            userInfo:(NSDictionary *)userInfo
            delegate:(id <PARTYKitDelegate>)delegate;

- (void)setRecognitionMode:(PKRecogitionMode)mode;

- (void)notifyConnectionDelegate;

- (void)sendVoicemailMessage:(NSString *)senderNumber receiver:(NSString *)receiverNumber;
/*!
 @abstract This method disconnect all PARTYKit subsystems.
 
 @discussion This method frees the resources of the PARTYKit framework and
 tears down any existing connections to the server.  It is usually not necessary
 to call this function, however, if you need to connect to a different server or
 port, it is necessary to call this function before calling setupWithKEY again.
 You must wait for the destroyed delegate method of the PARTYKitDelegate
 protocol to be called before calling setupWithKEY again.  Note also you will need
 to call setEarcon again to set up all your earcons after using this method.
 This function should NOT be called during an active recording or vocalization -
 you must first cancel the active transaction.  Calling it while the system is
 not idle may cause unpredictable results.
 */
- (void)destroy;

/*!
 @abstract This method start and end detection for recognize
 
 @discussion This method will start the recorder and record voice for recognize with
 PARTY server and your will get result of recognize with delegate pleace ensure your
 have set delegate in getter view
 */
- (void)startDetection;
- (void)endDetection;

/*!
 @abstract This method set simplerate for recognize
 
 @discussion This method will set simplerate for recognize
 PARTY server is contains 2 type 8000, 16000
 */
- (BOOL) isDectection;
- (BOOL) isConnected;

- (float) averagePower;
- (float) peakPower;

- (AudioQueueRef)getAudioQueue;
- (NSString *)getSoundPath;

@end


@protocol PARTYKitDelegate <NSObject>

@optional

- (void)recognizerWillConnecting:(PARTYKit *)recognizer;
- (void)recognizerDidConnected:(PARTYKit *)recognizer;

- (void)recognizerOnPeakPowerChangedTo:(float)peakPower;

- (void)recognizerFinishSendingPackage:(PARTYKit *)recognizer;
/*!
 @abstract Sent when the PARTYKit receive some package
 
 @discussion This will send NSDictionary to delegate
 in object can be 2 type Voicemail, ASR
 in Voicemail your can receive status and filename of voicemail
 in ASR can receive status and result of recognize
 */
- (void)recognizer:(PARTYKit *)recognizer didReceiveFilename:(PKFileResponse *)response;
- (void)recognizer:(PARTYKit *)recognizer didReceiveResult:(PKRecognizeResponse *)response;

/*!
 @abstract Sent when the PARTYKit receive error message
 
 @param error is type of PKErrorType
 
 @discussion This will send type and message of PKErrorType
 */
- (void)recognizer:(PARTYKit *)recognizer didError:(PKError *)error ;

/*!
 @abstract Sent when the destruction process is complete.
 
 @discussion This allows the delegate to monitor the destruction process.
 Note that subsequent calls to destroy and setupWithID will be ignored until
 this delegate method is called, so if you need to call setupWithID
 again to connect to a different server, you must wait for this.
 */
- (void)recognizerDidDestroyed:(PARTYKit *)recognizer;


@end
