//
//  PARTYKit_ExampleTests.m
//  PARTYKit_ExampleTests
//
//  Created by Kiettiphong Manovisut on 7/1/14.
//  Copyright (c) 2014 SPTSPEECH. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface PARTYKit_ExampleTests : XCTestCase

@end

@implementation PARTYKit_ExampleTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
